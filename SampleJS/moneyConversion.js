<!DOCTYPE html>
<html>
<body>

<h2>JavaScript in Body</h2>

<p id="demo1"></p>
<p id="demo2"></p>
<p id="demo3"></p>

<script>
var currencyUS = "-$4,400,400.50";
var currencyEU = "-5.500.500,40 EUR";
var resultUS = Number(currencyUS.replace(/[^0-9.-]+/g,""));
var resultEU = Number(currencyEU.replace(/[^0-9,-]+/g,"").replace(/[,]+/g,"."));


a = "my Stuff";
document.getElementById("demo1").innerHTML = resultUS;
document.getElementById("demo3").innerHTML = "my result is: " + resultEU;

</script>

</body>
</html> 
