import os

source_folder = "path/to/source/folder"
target_folder = "path/to/target/folder"
results_folder = "path/to/results/folder"

# Create the results folder if it doesn't exist
if not os.path.exists(results_folder):
    os.makedirs(results_folder)

# Initialize the lists for files that don't exist and files that don't match
files_not_exist = []
files_not_match = []

# Iterate over each file in the source folder
for file_name in os.listdir(source_folder):
    source_file_path = os.path.join(source_folder, file_name)
    target_file_path = os.path.join(target_folder, file_name)

    # Check if the file exists in the target folder
    if not os.path.exists(target_file_path):
        files_not_exist.append(file_name)
    else:
        # Compare the contents of the files
        with open(source_file_path, 'r') as source_file, open(target_file_path, 'r') as target_file:
            source_content = source_file.read()
            target_content = target_file.read()

            if source_content != target_content:
                files_not_match.append(file_name)

# Write the results to the respective text files
with open(os.path.join(results_folder, "results_does_not_exist.txt"), 'w') as not_exist_file:
    not_exist_file.write("\n".join(files_not_exist))

with open(os.path.join(results_folder, "results_does_not_match.txt"), 'w') as not_match_file:
    not_match_file.write("\n".join(files_not_match))