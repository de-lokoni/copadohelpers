import os
import shlex
import subprocess
import time
import json

sfAlias = 'qaAPOsandbox'
numberOfScriptRuns = 102
apexScriptOnePath = '/Users/konstantinheidt/Desktop/DevSources/CopadoHelpers/PythonStuff/ExecuteApexScript/script.apex'

#the rest is optional, use if you have ore complex use cases
basePath = '/Users/konstantinheidt/Desktop/DevSources/qa-source-mass-metadata_github'
apexScriptTwo = 'scripts/DataDeployStress_GenerateRecords_AllOtherObjects.apex'
apexScriptTwoPath = os.path.join(basePath, apexScriptTwo)
replaceLine = '___nr___'
tempDir = '/Users/konstantinheidt/Desktop/DevSources/qa-source-mass-metadata_github/RecordGenerator/TempFile'
waitFor = 10

def runScriptCommand(filePath,targetOrg):
    runScriptCommand = 'sf run apex --json --file=' + filePath + ' --target-org=' + targetOrg
    args = shlex.split(runScriptCommand)
    #print(args)
    process = subprocess.run(args, capture_output=True,bufsize=0)
    return process

#optional
def getFile(filePath):
    with open(filePath, "r+") as file:
        script = file.read()
    return script

#optional
def storeFile(filePath, content):
    with open(filePath, "w") as file:
        file.write(content)
    file.close()

def runScript(numberOfRuns,scriptPath,orgAlias):
    for i in range(0, numberOfRuns):
        commandResult = runScriptCommand(scriptPath,orgAlias)
        resultBody = commandResult.stdout
        jsonResult = json.loads(resultBody)
        if(jsonResult["result"]["success"] == True):
            print('done round ' + str(i+1) + ". wait " + str(waitFor) + ' seconds and continue ' + str(numberOfRuns-(i+1)) + ' more times' )
            time.sleep(10)
        else:
            print('stuff broke. error: ' + jsonResult["result"]["exceptionMessage"])
            break

    print('finished script execution. check results')

print(sfAlias)
print(apexScriptOnePath)
#print(apexScriptTwoPath)
runScript(numberOfScriptRuns,apexScriptOnePath,sfAlias)
print('finished script execution')