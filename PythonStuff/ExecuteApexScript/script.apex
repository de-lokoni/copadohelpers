/**
we need some ways to exclude users from being scrambled:
List of Emails
List of UserNames
Users in a Group
Maybe a do not scramble field in the future.
Also, let's set some base variables for the scrambling part.
**/
String sFirstName = 'some';
String sLastName = 'user';
String sEmailDomain = '@copado.com.invalid';
String sUserNameDomain = 'copado.com.qaApoSBX';
String skipUsersGroupName = 'do_not_scramble_users_group';
String scrambledIndicator = 'scrambled';
String likeSearch = scrambledIndicator + '%';
Integer nrOfUsersToBeScrambled = 10;

Set<String> skipUserNames = new Set<String>{
    'success@customer-success.force.com'
};
            
Set<String> skipEmails = new Set<String>{
    'integration.user+guestusersuccesssite@copado.com.invalid',
    'noreply@salesforce.com.invalid',
    'pdhamale@copado.com.invalid',
    'irosengren@copado.com.invalid',
    'nnatha@copado.com.invalid',
    'dvazquez@copado.com.invalid',
	'dyenda@copado.com.invalid'
};
                                 
/**
Get the users, apply logic for skipping and then scramble the values
IF we apply the limit in thre query, we don't know how many users are left to be scrambled 
which makes it difficult to create a unique number for each scrambled user.
So we first query all, assuming it's less than 50k.
**/
List<User> scopeUsers = new List<User>([
    SELECT Id,FirstName,LastName,UserName,Email,Alias,IsActive, FederationIdentifier, CommunityNickname  
    FROM User 
	WHERE 
        (NOT FederationIdentifier LIKE :likeSearch) AND
    	Id NOT IN
        (
            SELECT UserOrGroupId
            FROM GroupMember
            WHERE Group.Name = :skipUsersGroupName
        ) AND
    	Id NOT IN :skipEmails AND
    	ID NOT IN :skipUserNames AND
        Email LIKE '%.invalid'
]);

Integer usersLeftToScramble = scopeUsers.size();

/**
there can be duplicates. So we need to compare to users already created.
**/
List<User> doneUserList = new List<User>([
    SELECT Id, Username
    FROM User
    WHERE FederationIdentifier LIKE :likeSearch
]);

Map<String,User> doneUserMap = new Map<String, User>(); 
for(User u : doneUserList){
    doneUserMap.put(u.Username,u);
}
System.debug(doneUserMap.keyset().size() + 'users have been scrambled already');
System.debug(usersLeftToScramble + ' Users are left to be scrambled.');
System.debug(nrOfUsersToBeScrambled + ' of those Users will be scrambled.');
/**
now, this script cannot do all users at once. it's running into an apex timeout limit.
However, we need to create "unique" usernames. 
So for now the idea is to count down how many are left and put this number as part of the scrambling.
Hey, it's a start. 
UID calculation might need adjustment, such as -skipEmail and so on, but we will see.
Also, no activation/deactivation. That can be used in another script.
**/

List<User> scrambledUsers = new List<User>();
for(Integer i = 0;i< nrOfUsersToBeScrambled ;i++){    
    User u = scopeUsers[i];
    
    if(
        !skipEmails.contains(u.Email) &&
        !skipUserNames.contains(u.Username) 
    ){        
		System.debug('bef-scramble: ' + u);
        String uid = String.valueOf(usersLeftToScramble-2-i);
        u.FirstName = sFirstName ;
        u.LastName = sLastName + uid;
        u.UserName = sFirstName + '.' + sLastName + '.'+ uid + '@'+ sUserNameDomain;
        u.Alias = sFirstName.left(1) + sLastName.left(1) + uid;
        u.CommunityNickname = u.Alias;
        //u.IsActive = false;
        u.Email = sFirstName.left(1) +  sLastName + uid + sEmailDomain;
        u.FederationIdentifier =  scrambledIndicator + '.'+ u.Username;
        System.debug('aft-scramble: ' + u);
        
        scrambledUsers.add(u);
    }
}

Database.update(scrambledUsers);