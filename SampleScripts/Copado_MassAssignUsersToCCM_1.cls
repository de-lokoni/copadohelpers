
//get users with a copado installed package license

// String customDomain = '';

Map<Id, User> users =  new Map<Id, User>([
    SELECT ID,UserRole.Name,Profile.Name,IsActive, Username
    FROM User 
    WHERE  
    	Id IN (SELECT UserId FROM UserPackageLicense WHERE PackageLicense.NamespacePrefix = 'copado')
]);
 
System.debug(users);

/** used for other logic
Map<Id,User> copadoUserMap = new Map<Id, User>();
for(User u:users){
    copadoUserMap.put(u.Id, u);
}


Map<Id, copado__Org__c> orgCredMap = new Map<Id,copado__Org__c>();

**/

/**
* This script is tweaked towards assigning CCM Licenses to Developers as this is the large bulk.
**/
copado.GlobalAPI copadoGlobalAPI = new copado.GlobalAPI(); 
copado.GlobalAPI.CopadoLicenses info = copadoGlobalAPI.getLicenseInformation();

Integer totalCCM = info.totalNumberOfCCMLicenses;
Integer remainingCCM = info.availableCCMLicenses;

System.debug(info);

List<copado.GlobalAPI.UserLicense> licenseList = copadoGlobalAPI.listCopadoLicenses();
Set<Id> licensedUsers = new Set<Id>();
Set<Id> unlicensedUsers = new Set<Id>();

//check for existing users
for(integer i=0; i<licenseList.size();i++){
    
    //Although you remove a license, the record keeps saved in the background. 
    //to cater for users which have gotten their licenses removed previously, this check is required.
    System.debug(licenseList[i]);
    Boolean isCADenabled = licenseList[i].isCADenabled;
    Boolean isCCHenabled = licenseList[i].isCCHenabled;
    Boolean isCCMenabled = licenseList[i].isCCMenabled;
    Boolean isCopadoEnabled = licenseList[i].isCopadoEnabled;
    Boolean isCSTenabled = licenseList[i].isCSTenabled;
    
    //construct sets to be used later
    if(!isCADenabled && !isCCHenabled && !isCCMenabled && !isCopadoEnabled && !isCSTenabled){
        unlicensedUsers.add(licenseList[i].userId);
    } else {
        licensedUsers.add(licenseList[i].userId);
    }
}

//count licenses
Integer numlic = 0;

//assign copado ccm license to users in the managed package
for(User u: users.values()){
	//for users with licenses
    if(licensedUsers.contains(u.Id)){
        System.debug('the user ' + u.Username + ' has already a license; user Id: ' + u.Id);
    }

    //for users without licenses
    else if(unlicensedUsers.contains(u.Id) || !licensedUsers.contains(u.id)){
        numlic += 1;
        if(numlic<=remainingCCM){
            copado.GlobalAPI.UserLicense newLicense = new copado.GlobalAPI.UserLicense(
                u.Id, //Id userId, 
                false, //Boolean isCADenabled for Agile only users, 
                false, //Boolean isCCHenabled for Compliance Hub users, 
                true, //Boolean isCCMenabled for Developers, 
                false, //Boolean isCopadoEnabled for Release Managers or users managing Copado, 
                false //Boolean isCSTenabled for users managing Test Automation
            );
            
            System.debug('a license will be created for: ' + u.Username);
            copadoGlobalAPI.upsertCopadoLicense(u.Id,newLicense);
        } else{
            System.debug('there are no licenses for ' + u.Username);
        }
    }
}