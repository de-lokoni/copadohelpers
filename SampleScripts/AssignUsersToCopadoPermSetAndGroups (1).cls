/**
List<UserPackageLicense> packageLicenses = new List<UserPackageLicense>();

//get package license for copado
PackageLicense pl = [SELECT Id FROM PackageLicense WHERE NamespacePrefix = 'copado'];
**/

//get users with a copado installed package license
List<User> copadoUsers =  [
    SELECT ID,UserRole.Name,Profile.Name,IsActive 
    FROM User 
    WHERE  
    	Id IN (SELECT UserId FROM UserPackageLicense WHERE PackageLicense.NamespacePrefix = 'copado')
];
 
System.debug(copadoUsers);

Set<Id> copadoUserIds = new Set<Id>();
for(User u : copadoUsers){
    copadoUserIds.add(u.Id);
}

System.debug(copadoUserIds.size());

//assign perm sets  
List<PermissionSetAssignment> permissionSetList = new List<PermissionSetAssignment>();

PermissionSet copadoDevPS = [
    SELECT Id 
    FROM PermissionSet 
    WHERE Name = 'Copado_Developer'];
    
for (User u : copadoUsers){ 
    PermissionSetAssignment psa = new PermissionSetAssignment (PermissionSetId = copadoDevPS.Id, AssigneeId = u.Id);
    permissionSetList.add(psa);
}
try{
    Database.upsert(permissionSetList, PermissionSetAssignment.Id, false);
}catch(exception e){
    system.debug('exception caught' + e);
}

//assign Groups:
Group copadoDevs = [
    SELECT Id 
    FROM Group 
    WHERE Name = 'Copado Developers'];
    
for (User u : copadoUsers){ 
    GroupMember gm = new GroupMember (GroupId = copadoDevs.Id, UserOrGroupId  = u.Id);
    groupMembers.add(gm);
}

try{
    Database.upsert(groupMembers, GroupMember.Id, false);
}catch(exception e){
    system.debug('exception caught' + e);
}
