//get available namespace prefixes
//get permission sets
//get users in group
//assign perm sets to users

//let's go

/** Input Parameters */
String scopeGroupName = 'test14_standard_platform';
List<String> scopeNameSpaces = new List<String>{'copado', 'cmcSf','copadoQuality','copadoconnect'};
Set<String> skipPermissionSets = new Set<String>{'Encrypted_fields','Persona_Permission_Manager'};
//List<String> scopeNameSpaces = new List<String>{'copado', 'cmcSf','copadoQuality','copaCchPresets','copado_labs','copadoconnect', 'cmcMC'};

/** assign licenses first. De-dup needed */
Map<Id,PackageLicense> plMap = new Map<Id,PackageLicense>([
    SELECT Id,Status,UsedLicenses, AllowedLicenses, NamespacePrefix 
    FROM PackageLicense 
    WHERE Status = 'Active' AND 
    NamespacePrefix IN :scopeNameSpaces AND 
    AllowedLicenses != -1
]);

List<UserPackageLicense> existingUPLList = new List<UserPackageLicense>([
    SELECT Id,UserId, PackageLicenseId 
    FROM UserPackageLicense 
    WHERE PackageLicenseId IN :plMap.keySet()
]);

Set<String> existingUserPackageLicenseKey = new Set<String>();
for(UserPackageLicense upl : existingUPLList){
    String key = 'UPL_' + String.valueOf(upl.UserId) + String.valueOf(upl.PackageLicenseId);
    existingUserPackageLicenseKey.add(key);
}

List<PermissionSet> permSets = new List<PermissionSet>(
    [SELECT Id, Name, Label, ProfileId, LicenseId, IsCustom, NamespacePrefix 
    FROM PermissionSet 
    WHERE NamespacePrefix IN :scopeNameSpaces AND Name NOT IN :skipPermissionSets
    ]);

Group scopeGroup = [SELECT Id, Name FROM Group WHERE Name =: scopeGroupName ORDER BY LastModifiedDate DESC LIMIT 1];

Map<Id,User> usersInGroup = new Map<Id,User>([
    SELECT Id, UserName 
    FROM User
    WHERE Id IN (
        SELECT UserOrGroupId FROM GroupMember WHERE GroupId = :scopeGroup.Id
    )
]);

//query existing for de-duplication
List<PermissionSetAssignment> existingAssignments = new List<PermissionSetAssignment>([
    SELECT Id, AssigneeId, PermissionSetId 
    FROM PermissionSetAssignment
    WHERE PermissionSetId IN :permSets
]);

Set<String> permSetUserKey = new Set<String>();
for(PermissionSetAssignment pas :existingAssignments){
    String key = 'PSA_'+String.valueOf(pas.AssigneeId) + String.valueOf(pas.PermissionSetId);
    permSetUserKey.add(key);
}

Map<String,PermissionSetAssignment> psas = new Map<String,PermissionSetAssignment>();
Map<String,UserPackageLicense> upls = new Map<String,UserPackageLicense>();
Integer countRows = 0;
for(PermissionSet ps : permSets){
    for(User u : usersInGroup.values()){

        String key = 'PSA_'+String.valueOf(u.Id) + String.valueOf(ps.Id);
        if(permSetUserKey.contains(key)){
            System.debug('user already added');
        } else{
            //check for proper license assignment and add license if needed
            for(PackageLicense pl : plMap.values()){
                String plKey = 'UPL_'+String.valueOf(u.Id) + pl.Id;
                
                if(!existingUserPackageLicenseKey.contains(plKey)){
                    UserPackageLicense upl = new UserPackageLicense();
                    upl.UserId = u.Id;
                    upl.PackageLicenseId = pl.Id;
                    
                    if(upls.get(plKey) == null){
                        upls.put(plKey, upl);
                        System.debug('added user: ' + u.UserName + ' to package ' + pl.NamespacePrefix);
                    }
                } else{
                    System.debug('user already has license ' +  u.UserName + ' to package ' + pl.NamespacePrefix);
                }
            }

            PermissionSetAssignment pas = new PermissionSetAssignment();
            pas.AssigneeId = u.Id;
            pas.PermissionSetId = ps.Id;
            if(psas.get(key)==null){
                psas.put(key,pas);
                System.debug('assignment nr: ' + countRows + ' added user: ' + u.UserName + ' to PermissionSet ' + ps.Name);
				countRows += 1;
            }
        }
    }
}

System.debug('Package Licenses to assign: ' + upls.size());
System.debug('Permission Sets to assign: ' + psas.size());

Database.upsert(upls.values());
Database.upsert(psas.values());
