
/**
Script to Pre-create org credentials and change owner so that users only need to authenticate their org creds when logging in the first time
**/

//Domain type. change if required. provide custom domain if required
String customDomain = 'https://customerName.my.salesforce.com';
String environmentType = 'Custom Domain';
String orgName = 'Production';

//Users in Scope. change query if required
Map<Id, User> copadoUserMap =  new Map<Id, User>([
    SELECT ID,UserRole.Name,Profile.Name,IsActive, Username, Alias
    FROM User 
    WHERE  
    Id IN (
        SELECT UserId 
        FROM UserPackageLicense 
        WHERE PackageLicense.NamespacePrefix = 'copado'
    )
]);

System.debug(copadoUserMap.values());


Map<Id, copado__Org__c> orgCredMap = new Map<Id,copado__Org__c>();

for(User u:copadoUserMap.values()){
    
    copado__Org__c org = new copado__Org__c();
    org.Name = u.Alias + '_' + orgName;
    org.copado__Org_Type__c = environmentType;
    if(environmentType == 'Custom Domain'){
        org.copado__Custom_Domain__c = customDomain;
    }
    org.OwnerId = u.Id;
    orgCredMap.put(u.Id, org);
}

try{
    Database.upsert(orgCredMap.values(), copado__Org__c.Id, false);
}catch(exception e){
    system.debug('exception caught' + e);
}

